<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Building extends Model
{
    protected $fillable = [
        'name','organization_id'
    ];
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
    public function floor(){
        return $this->hasMany(Floor::class);
    }
}
