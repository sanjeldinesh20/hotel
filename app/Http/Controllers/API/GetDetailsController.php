<?php

namespace App\Http\Controllers\API;

use App\Job;
use App\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetDetailsController extends Controller
{
    public function getOrganizationData(Request $request){
        return Organization::whereBetween('created_at', [$request->from, $request->to])->get();
    }

    public function getAllData(){
        return Organization::select()->with('user')->get();
    }
}
