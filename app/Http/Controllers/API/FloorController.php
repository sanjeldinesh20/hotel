<?php

namespace App\Http\Controllers\API;

use App\Floor;
use App\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FloorController extends Controller
{
    public function edit(Request $request,$id){
        $floor= Floor::create([
            'building_id'=>$id,
            'name'=>$request->floorName
        ]);
        for($i=1;$i<=$request->noofRooms;$i++){
            Room::create([
                'floor_id'=>$floor->id,
                'name'=>"Room".$i
            ]);
        }
//        $count= count($floor->where('building_id',$id)->get());
//        $floor->update([
//            'name'=>$request->data
//        ]);
        return $floor->with('room')->latest('id')->first();

    }
    public function destroy($id){
        Floor::where('id',$id)->delete();
        return "Floor Deleted.";
    }
}
