<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = [
        'name', 'room_number', 'floor_id', 'room_type' , 'description'
    ];
}
