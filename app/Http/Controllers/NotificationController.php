<?php
namespace App\Http\Controllers;
use App\Notification;
use Illuminate\Http\Request;
class NotificationController extends Controller
{
    public function notification(){
        $not= Notification::select();
        return $not->orderBy('created_at','Desc')->get();
    }
}
