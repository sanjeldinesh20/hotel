<?php

namespace App\Http\Controllers\API;

use App\CheckIn;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class CheckinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return CheckIn::where('user_id', $request->userid)->latest()->paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return CheckIn::create([
            'user_id' => $request['userid'],
            'intime' => \Carbon\Carbon::now()->format('Y-m-d H:i'),
            'status'=>"true"
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }

    public function list()
    {
        return CheckIn::with('user')->latest()->paginate(10);
    }
public function testCheckin(){
    
    return CheckIn::where('id',auth('api')->user()->id)->latest('status')->first();
}
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }
    


    public function checkout(Request $request, $id)
    {
        $checkin = CheckIn::find($id);
        $checkin->outtime = \Carbon\Carbon::now()->format('Y-m-d H:i');
        $checkin->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
