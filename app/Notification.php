<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Notification extends Model
{
    protected $fillable=['name','user_id'];

    public function notifications(){
        return $this->belongsTo(User::class);
    }
}
