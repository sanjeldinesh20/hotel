<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcastNow;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class SendNotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $user;
    public $message;

    /**
     * Create a new event instance.
     *
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->message="{$user} has logged";
//        dd($this->user);

    }

    /**
     * Get the channels the event should broadcast on.
     *app.js
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('user');
    }

    public function broadcastAs() {

        return 'event';

    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
//    public function broadcastWith()
//    {
//        return ['id' => $this->user->id];
//    }
}
