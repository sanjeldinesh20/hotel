import Vue from 'vue'
import Vuex from 'vuex'
import axios from "axios";

Vue.use(Vuex)
const state = {
    tableData:[]
}
const mutations = {

    setData(state,payload){
        state.tableData=payload
    },
    setOrganization(state,payload){
        state.tableData=payload
        console.log(payload)
    }
}
const actions = {
    async getOrganizationRecords({commit},payload) {
        let response=await axios.post('/api/getdata',payload)
        console.log(response.data)
        commit('setOrganization',response.data)
    },
    async getData({commit}){
       let response= await axios.post('/api/get-all')
        commit('setData',response.data)
    }
}
const getters = {
tableData:state=>state.tableData
}
export default new Vuex.Store({
    state,
    mutations,
    actions,
    getters
})


