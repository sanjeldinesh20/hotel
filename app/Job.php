<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable=[
        'organization_id'
    ];
    public function organization(){
        return $this->belongsTo(Organization::class);
    }
}
