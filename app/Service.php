<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Service extends Model
{
    protected $fillable = [
        'room_number',
        'remark',
        'user_id'
    ];
    protected $casts=[
        'room_number' => 'array'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
