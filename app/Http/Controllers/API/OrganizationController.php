<?php

namespace App\Http\Controllers\API;

use App\Building;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Organization;
use Auth;


class OrganizationController extends Controller
{
    public $user_id;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return Organization::select()->with('user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        if($request->user_type=="Admin"){
            $user_id=User::select('id')->where('type',$request->user_type)->first()->id;
        }
        else{
            $user_id=User::select('id')->where('name',$request->selected)->first()->id;
        }
       $org= Organization::create([
            'name' =>$request->name,
            'user_id'=>$user_id,
            'contact'=>$request->contact,
            'location'=>$request->location,
            'email'=>$request->email,
            'scheduled_date'=>$request->date,
            'status'=>$request->status,
           'floors'=>$request->floors,
           'rooms'=>$request->rooms
        ]);
for ($i=1;$i<=$request->buildings;$i++){
   $building= Building::create([
        'organization_id'=>$org->id,
        'name'=>"B".$i
    ]);


}

//        return $org->user()->with('organizations')->get();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $org=Organization::findOrFail($id);

         return $org->with(['user','building.floor.room'])->get();


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $organization = Organization::findOrFail($id);


              $organization->update($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('isAdmin');

            $organization = Organization::findOrFail($id);
            $organization->delete();

            return ['message' => 'organization Deleted'];


    }
}
