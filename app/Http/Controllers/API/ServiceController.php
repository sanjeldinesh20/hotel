<?php
namespace App\Http\Controllers\API;
use App\Service;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return Service::where('user_id',$request->userid)->latest()->paginate(10);
    }
    public function store(Request $request)
    {
        /* $this->validate($request,[
            'name'=>'required|string|max:191',
             'email'=>'required|string|email|max:191|unique:users',
             'phone'=>'required|unique:users',
         ]);*/
        // dd($request['room_number']);
        // dd(Auth::check());
        return Service::create([
            'room_number' =>$request['room_number'],
            'user_id' => $request['userid'],
            'remark' =>$request['remark'],
        ]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::find0rFail($id);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrFail($id);
        /*  $this->validate($request,[
              'name'=>'required|string|max:191',
              'email'=>'required|string|email|max:191|unique:users,email'.$user->id,
              'phone'=>'sometimes|unique:users',
          ]);*/
        $service->update($request->all());
        //  return ['message' => 'User update'];
        //     return $id;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        $service->delete();
//            return ['message' => 'User Deleted'];
    }
    public function showservices($id)
    {
        // dd(1);
        return User::find($id)->services;

    }
    public function allServices(){
        return Service::all();
    }

}


