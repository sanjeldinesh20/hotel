(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[10],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userDetail.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/userDetail.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      user: "",
      services: "",
      userid: "",
      id: this.$route.params.id
    };
  },
  mounted: function mounted() {
    // this.userid = $('meta[name="user_id"]').attr('content');
    this.getuser();
    this.getuserservice(); // axios.get(`/api/service/?userid=${this.userid}`).then(response =>{
    //   this.services = response
    //   console.log(this.services)
    // })
  },
  methods: {
    getuser: function getuser() {
      var _this = this;

      axios.get("/api/user/" + this.id).then(function (response) {
        _this.user = response.data;
        console.log(_this.user);
      });
    },
    getuserservice: function getuserservice() {
      var _this2 = this;

      axios.get("/api/service/" + this.id).then(function (response) {
        console.log(response.data);
        _this2.services = response.data;
      });
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userDetail.vue?vue&type=template&id=92e1a2f2&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/userDetail.vue?vue&type=template&id=92e1a2f2& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "card" }, [
    _vm._m(0),
    _vm._v(" "),
    _c("div", { staticClass: "card-body" }, [
      _c("div", { staticClass: "row" }, [
        _c(
          "div",
          { staticClass: "col-12 col-md-12 col-lg-8 order-2 order-md-1" },
          [
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "row" }, [
              _c(
                "div",
                { staticClass: "col-12" },
                [
                  _vm._m(2),
                  _vm._v(" "),
                  _vm._l(_vm.services, function(service) {
                    return _c(
                      "div",
                      { key: service.id, staticClass: "post" },
                      [
                        _c("div", { staticClass: "user-block" }, [
                          _vm._m(3, true),
                          _vm._v(" "),
                          _c("h1", { staticClass: "text-default" }, [
                            _vm._v(_vm._s(service.created_at))
                          ])
                        ]),
                        _vm._v(" "),
                        _vm._m(4, true),
                        _vm._v(" "),
                        _vm._l(service.room_number, function(
                          room_number,
                          index
                        ) {
                          return _c(
                            "h1",
                            { key: index, staticClass: "text-default" },
                            [
                              _c("div", { staticClass: "row" }, [
                                _c("div", { staticClass: "col-md-2" }, [
                                  _vm._v(_vm._s(room_number))
                                ])
                              ])
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _vm._m(5, true),
                        _vm._v(" "),
                        _c("div", { staticClass: "callout callout-danger" }, [
                          _c("p", [_vm._v(_vm._s(service.remark))])
                        ])
                      ],
                      2
                    )
                  })
                ],
                2
              )
            ])
          ]
        ),
        _vm._v(" "),
        _c(
          "div",
          { staticClass: "col-12 col-md-12 col-lg-4 order-1 order-md-2" },
          [
            _c("h3", { staticClass: "text-primary" }, [
              _c("i", { staticClass: "fas fa-user" }),
              _vm._v(
                "\n                    " +
                  _vm._s(_vm.user.name) +
                  "\n                "
              )
            ]),
            _vm._v(" "),
            _c("p", { staticClass: "text-muted" }, [
              _vm._v(_vm._s(_vm.user.type))
            ]),
            _vm._v(" "),
            _c("br"),
            _vm._v(" "),
            _c("div", { staticClass: "text-muted" }, [
              _c("p", { staticClass: "text-sm" }, [
                _vm._v(
                  "\n                        Email Address\n                        "
                ),
                _c("b", { staticClass: "d-block" }, [
                  _vm._v(_vm._s(_vm.user.email))
                ])
              ]),
              _vm._v(" "),
              _c("p", { staticClass: "text-sm" }, [
                _vm._v(
                  "\n                        Phone Number\n                        "
                ),
                _c("b", { staticClass: "d-block" }, [
                  _vm._v(_vm._s(_vm.user.phone))
                ])
              ])
            ])
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "card-header" }, [
      _c("h3", { staticClass: "card-title" }, [_vm._v("Projects Detail")]),
      _vm._v(" "),
      _c("div", { staticClass: "card-tools" }, [
        _c(
          "button",
          {
            staticClass: "btn btn-tool",
            attrs: {
              type: "button",
              "data-widget": "collapse",
              "data-toggle": "tooltip",
              title: "Collapse"
            }
          },
          [_c("i", { staticClass: "fas fa-minus" })]
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "row" }, [
      _c("div", { staticClass: "col-12 col-sm-4" }, [
        _c("div", { staticClass: "info-box bg-light" }, [
          _c("div", { staticClass: "info-box-content" }, [
            _c(
              "span",
              { staticClass: "info-box-text text-center text-muted" },
              [_vm._v("Estimated budget")]
            ),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "info-box-number text-center text-muted mb-0" },
              [_vm._v("2300")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-sm-4" }, [
        _c("div", { staticClass: "info-box bg-light" }, [
          _c("div", { staticClass: "info-box-content" }, [
            _c(
              "span",
              { staticClass: "info-box-text text-center text-muted" },
              [_vm._v("Total amount spent")]
            ),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "info-box-number text-center text-muted mb-0" },
              [_vm._v("2000")]
            )
          ])
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "col-12 col-sm-4" }, [
        _c("div", { staticClass: "info-box bg-light" }, [
          _c("div", { staticClass: "info-box-content" }, [
            _c(
              "span",
              { staticClass: "info-box-text text-center text-muted" },
              [_vm._v("Estimated project duration")]
            ),
            _vm._v(" "),
            _c(
              "span",
              { staticClass: "info-box-number text-center text-muted mb-0" },
              [_vm._v("\n                20\n                "), _c("span")]
            )
          ])
        ])
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "alert alert-info alert-dismissible" }, [
      _c("h5", [
        _c("i", { staticClass: "icon fas fa-info" }),
        _vm._v(" Recent Activites")
      ]),
      _vm._v(
        "\n                            All works done!!\n                        "
      )
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h3", { staticClass: "text-default" }, [
      _c("i", { staticClass: "fas fa-calendar-alt" }),
      _vm._v(" Date\n                                ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h2", { staticClass: "text-default" }, [
      _c("i", { staticClass: "fas fa-door-open" }),
      _vm._v(" Serve Room Number\n                            ")
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("h5", { staticClass: "text-default" }, [
      _c("i", { staticClass: "fas fa-pen-nib" }),
      _vm._v("Remark\n                            ")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/userDetail.vue":
/*!************************************************!*\
  !*** ./resources/js/components/userDetail.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _userDetail_vue_vue_type_template_id_92e1a2f2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./userDetail.vue?vue&type=template&id=92e1a2f2& */ "./resources/js/components/userDetail.vue?vue&type=template&id=92e1a2f2&");
/* harmony import */ var _userDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./userDetail.vue?vue&type=script&lang=js& */ "./resources/js/components/userDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _userDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _userDetail_vue_vue_type_template_id_92e1a2f2___WEBPACK_IMPORTED_MODULE_0__["render"],
  _userDetail_vue_vue_type_template_id_92e1a2f2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/userDetail.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/userDetail.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/userDetail.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_userDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./userDetail.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userDetail.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_userDetail_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/userDetail.vue?vue&type=template&id=92e1a2f2&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/userDetail.vue?vue&type=template&id=92e1a2f2& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_userDetail_vue_vue_type_template_id_92e1a2f2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./userDetail.vue?vue&type=template&id=92e1a2f2& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userDetail.vue?vue&type=template&id=92e1a2f2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_userDetail_vue_vue_type_template_id_92e1a2f2___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_userDetail_vue_vue_type_template_id_92e1a2f2___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);