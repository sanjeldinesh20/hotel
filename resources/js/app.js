/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
// import '@mdi/font/css/materialdesignicons.css' // Ensure you are using css-loader

// import Vuetify from 'vuetify'
// import 'vuetify/dist/vuetify.min.css'



// Vue.use(Vuetify)




import moment from 'moment';
import { Form, HasError, AlertError } from 'vform';

import Gate from "./Gate";
 Vue.prototype.$gate = new Gate(window.user);

import CarbonComponentsVue from "@carbon/vue/src/index";
Vue.use(CarbonComponentsVue);

import swal from 'sweetalert2'
window.swal = swal;
const toast = swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000
});
window.toast = toast;


window.Form = Form;
Vue.component(HasError.name, HasError)
Vue.component(AlertError.name, AlertError)

import VueRouter from 'vue-router'
Vue.use(VueRouter)
import VueProgressBar from 'vue-progressbar'
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '5px'
})
// import dashboard from './components/Dashboard.vue';
// import profile from './components/Profile.vue';

const dashboard = () => import('./components/Dashboard.vue')
const profile = () => import('./components/Profile.vue')
const users = () => import('./components/Users.vue')
const rooms = () => import('./components/Room.vue')
const loginfo = () => import('./components/LogInfo.vue')
const notfound = () => import('./components/NotFound.vue')
const userservices = () => import('./components/userService.vue')
const userdetail = () => import('./components/userDetail.vue')
const allServices = () => import('./components/allServices.vue')
const building = () => import('./components/building.vue')
const organization = () => import('./components/organization.vue')
const organizationShow = () => import('./components/organizationShow.vue')




let routes = [
    { path: '/home', component: dashboard },
    { path: '/', component: dashboard },
    { path: '/dashboard', component: dashboard },
    { path: '/users', component : users },
    { path: '/profile',  component : profile},
    { path: '/rooms',  component : rooms},
    { path: '/loginfo',  component : loginfo},
    { path: '/userservices',  component : userservices},
    { path: '*',  component : notfound},
    { path: '/userdetail/:id', component: userdetail},
    { path: '/allServices', component: allServices},
    { path: '/organization', component: organization},
    { path: '/building', component: building},
    { path: '/organization/show/:id', component: organizationShow},





]


const router = new VueRouter({
    mode: 'history',
    routes // short for `routes: routes`
})


Vue.filter('upText',function(text){
    return text.charAt(0).toUpperCase() + text.slice(1)
});

Vue.filter('newdata',function(created){
    return moment().format('MMMM Do YYYY');
});
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('Modal', require('./components/FloorModal.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);
Vue.component(
    'DashboardTable',
    require('./components/DashboardTable.vue').default
);
Vue.component(
    'Data',
    require('./components/Data.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'not-found',
    require('./components/NotFound.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

import store from "./store.js"


const app = new Vue({
    el: '#app',
    router,
    store
});
