<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = [
        'name','user_id','scheduled_date','location','contact','email','completed_on','status','floors','rooms'
    ];
    protected $casts=[
        'history'=>'array'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function building()
    {
        return $this->hasMany(Building::class);
    }
    public function job(){
        return $this->hasOne(Job::class);
    }
}
