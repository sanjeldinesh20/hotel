<?php

namespace App\Http\Controllers\API;

use App\Floor;
use App\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Building;

class BuildingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        return Building::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createNew(Request $request,$id)
    {

    $building= Building::create([
    'organization_id'=>$id,
    'name'=>$request->building
    ]);
//    $count= count($building->where('organization_id',$id)->get());
//    $building->update([
//        'name'=>"b".$count
//    ]);
        for($i=1;$i<=$request->floors;$i++){
            $floor= Floor::create([
                'building_id'=>$building->id,
                'name'=>"Floor".$i
            ]);
//            for($i=1;$i<=$request->rooms;$i++){
//                $room= Room::create([
//                    'floor_id'=>$floor->id,
//                    'name'=>"Room".$i
//                ]);
//        }
        }
        return $building->with('floor')->latest('id')->first();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        return Building::create([
//            'organization_id' =>$request['organization_id'],
//            'name' =>$request['name'],
//            'floor' =>$request['floor']
//        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        return Building::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//        $building = Building::findOrFail($id);
//
//
//              $building->update($request->all());



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//        $this->authorize('isAdmin');
//
//            $building = Building::findOrFail($id);
//            $building->delete();
//
//            return ['message' => 'building Deleted'];

        Building::where('id',$id)->delete();
        return "Building deleted.";



    }
}
