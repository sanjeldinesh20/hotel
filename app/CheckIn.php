<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CheckIn extends Model
{
    protected $fillable = [
        'intime', 'outtime', 'user_id','status'
    ];

    protected $appends=[
        'test','min'
    ];
    


    public function getTestAttribute(){
            $a = $this->intime;
            $b = $this->outtime;

        if ($this->intime && $this->outtime && $this->outtime > $this->intime)
            return Carbon::parse($this->outtime)->diff(Carbon::parse($this->intime))->format('%h hours');

    }
    public function getMinAttribute(){

        if ($this->intime && $this->outtime && $this->outtime > $this->intime)
         return  $min = Carbon::parse($this->intime)->diffInMinutes(Carbon::parse($this->outtime));
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
