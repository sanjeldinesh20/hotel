<?php

namespace App\Http\Controllers\API;

use App\Room;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Room::paginate(10);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $this->validate($request,[
            'name'=>'required|string|max:191',
             'number'=>'required|string|number|max:191|unique:rooms',


         ]);*/


       return Room::create([
            'name' =>$request['name'],
            'room_number' =>$request['room_number'],
            'floor' =>$request['floor'],
            'room_type' =>$request['room_type'],
            'description' =>$request['description'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        $room = Room::findOrFail($id);

        /*  $this->validate($request,[
              'name'=>'required|string|max:191',
              'number'=>'required|string|number|max:191|unique:rooms',

          ]);*/
       $room->update($request->all());

     //  return ['message' => 'Room update'];
        //     return $id
    }

    public function destroy($id)
    {
        $room = Room::findOrFail($id);

        $room->delete();
//            return ['message' => 'Room Deleted'];
    }
}
