<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//        return $request->user();
//    });
Route::get('/profile','API\UserController@profile');
Route::put('/profile','API\UserController@updateProfile');
Route::get('/services/{user}','API\UserController@showservices')->name('user.showservice');
Route::get('/allServices','API\ServiceController@allServices');

// Route::group(['middleware'=>'auth:api'],function(){

Route::apiResources(['user'=>'API\UserController']);
Route::apiResources(['room'=>'API\RoomController']);
Route::apiResources(['building'=>'API\BuildingController']);
Route::post('building/{id}','API\BuildingController@createNew');
Route::apiResources(['organization'=>'API\OrganizationController']);
Route::post('floor/edit/{id}','API\FloorController@edit');
Route::post('floor/delete/{id}','API\FloorController@destroy');


Route::apiResources(['service'=>'API\ServiceController']);
// });

Route::get('/profile','API\UserController@profile');
Route::put('/profile','API\UserController@updateProfile');
Route::get('/service/{user}','API\UserController@showservices')->name('user.showservices');
Route::get('/checkin/list','API\CheckinController@list')->name('checkin.list');
Route::get('/allServices','API\ServiceController@allServices');
Route::get('notification','NotificationController@notification');
Route::get('test-checkin','API\CheckinController@testCheckin');


Route::apiResources(['service'=>'API\ServiceController']);
Route::apiResources(['checkin'=>'API\CheckinController']);
Route::put('/checkout/{id}','API\CheckinController@checkout');
Route::post('getdata','API\GetDetailsController@getOrganizationData');
Route::post('get-all','API\GetDetailsController@getAllData');

